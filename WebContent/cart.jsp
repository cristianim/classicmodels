<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<c:if test="${empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
	<title>Carrito</title>
</head>

<style>



</style>
<body>

<div class="container-fluid">

	<div class="jumbotron jumbotron-fluid ">
	
		<p class="display-4">Carrito de la compra</p>
	
	</div>
	
</div>

<ul class="nav justify-content-center">

<c:choose>

	<c:when test="${not empty sessionScope.customer }">
	
		<li class="nav-item">
	
			<p class="nav-link btn-secondary">${sessionScope.customer }</p>
			
		</li>	
		
		<li class="nav-item"><a class="nav-link" href="catalogo.jsp">Volver al catalogo</a></li>

	</c:when>
	<c:otherwise>
	
		<li class="nav-item">
	
			<a class="nav-link" href="login.jsp" >Iniciar Sesion</a>
			
		</li>
		
		<li class="nav-item">
			
			<a class="nav-link" href="registro.jsp" >Registrarse</a>
			
		</li>

	</c:otherwise>

</c:choose>

</ul>	
	
<div class="container-fluid">

<div class="row">
<div class="col-md-5 mx-auto">
	
	<c:choose>
		<c:when test="${empty sessionScope.cart}">
			<p>No se han añadido productos al carrito</p>
		</c:when>
		<c:otherwise>
			<table class="table table-hover">
				<tr>
					<th scope="col">Producto</th>
					<th scope="col">Unidades</th>
					<th scope="col">Precio</th>
					<th scope="col">Importe</th>
					<!-- <th scope="col">Borrar</th> -->
				</tr>
				<c:forEach var="linea" items="${sessionScope.cart}">
					<tr>
					
						<td scope="row">${linea.value.productName}</td>
						<td>${linea.value.amount}</td>
						<td>${linea.value.price}</td>
						<td>${linea.value.amount * linea.value.price}</td>
						<!-- <td></td> -->
					</tr>
					
					
				</c:forEach>
			</table>
		</c:otherwise>		
	</c:choose>
	
</div>
</div>
</div>
	
</body>
</html>
