<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>
<%
if (session.getAttribute("usuario") != null)
	response.sendRedirect("catalogo.jsp");
	//response.sendRedirect("postregistro.jsp");
else {
	String firstName = request.getParameter("nombre");
	String lastName = request.getParameter("apellidos");
	String email = request.getParameter("email");
	if (firstName == null || lastName == null || email == null)
		response.sendRedirect("catalogo.jsp");
		//response.sendRedirect("postregistro.jsp");
	else {
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	
	<title>Post Registro</title>
	
</head>
<%
System.out.println("hola");
%>
<body>

<div class="container-fluid">

	<div class="jumbotron jumbotron-fluid ">
	
		<p class="display-4">Post Registro</p>
	
	</div>
	
</div>

<ul class="nav justify-content-center">

	<li class="nav-item"><a class="nav-link" href="login.jsp">Iniciar sesión</a></li>

	<li class="nav-item"><a class="nav-link" href="catalogo.jsp">Catalogo</a></li>

</ul>

<div class="container-fluid">

	<h1>
		Bienvenido a classicmodels <%=firstName%></h1>
	<p>
		Gracias por registrarte, sólo te queda un paso más para completar tu registro y tener acceso a
		todos nuestros Servicios, sigue las instrucciones que te hemos enviado
		a
		<%=email%></p>
		
	<p><strong>¿No te ha llegado el mensaje?</strong> Pulsa el botón de reenviar para que te volvamos a enviar el
		mensaje de confirmación</p>
	<form action="reenvio" method="post">
		<input type="hidden" name="email" value="<%=email%>" />
		<p>
			<input type="submit" value="Reenviar" />
		</p>
		
	</form>
	
</div>

</body>
</html>
<%
}
}
%>
