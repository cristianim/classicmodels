<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<c:if test="${not empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<title>Registro de Usuarios</title>
<script>
	function check(confirm) {
		if (confirm.value != document.getElementById('password').value) {
			confirm.setCustomValidity('Las contraseñas no coinciden');
		} else
			confirm.setCustomValidity('');
	}
</script>
</head>
<body>

<div class="container-fluid">

	<div class="jumbotron jumbotron-fluid ">
	
		<p class="display-4">Registro de usuario</p>
	
	</div>
	
</div>

<ul class="nav justify-content-center">

<c:choose>

	<c:when test="${not empty sessionScope.customer }">
	
		<li class="nav-item">
	
			<p class="nav-link btn-secondary">${sessionScope.customer }</p>
			
		</li>	
		
		<li class="nav-item">
		
			<a class="nav-link " href="cart.jsp">Carrito</a>
			
		</li>
		
		<li class="nav-item">
		
			<a class="nav-link" href="logout.jsp" >Cerrar Sesion</a>
			
		</li>

	</c:when>
	<c:otherwise>
	
		<li class="nav-item">
	
			<a class="nav-link" href="catalogo.jsp">Volver al catalogo</a>
			
		</li>

	</c:otherwise>

</c:choose>

</ul>
	
<hr/>

	<div class="container">
			<div class="row">
				<div class="col-md-5 mx-auto">
					<div class="myform form ">
						<form action="signup" method="post" onsubmit="check()">
							<div class="form-group">
								<label for="nombre">Nombre</label> <input
									type="text" name="nombre" class="form-control" id="nombre"
									aria-describedby="emailHelp" placeholder="Introduce tu nombre"
									required="required">
							</div>
							<div class="form-group">
								<label for="apellidos">Apellidos</label> <input
									type="text" name="apellidos" class="form-control" id="apellidos"
									aria-describedby="emailHelp"
									placeholder="Introduce los apellidos" required="required">
							</div>
							<div class="form-group">
								<label for="email">Email</label> <input
									type="email" name="email" class="form-control" id="email"
									aria-describedby="emailHelp" placeholder="Introduce el email"
									required="required">
							</div>
							<div class="form-group">
								<label for="password">Contraseña</label> <input
									type="password" name="password" id="password"
									class="form-control" aria-describedby="emailHelp"
									placeholder="Introduce contraseña" required="required">
							</div>
							<div class="form-group">
								<label for="password">Repite Contraseña</label> <input
									type="password" id="confirm"
									class="form-control" aria-describedby="emailHelp"
									placeholder="Introduce contraseña" required="required"
									oninput="check(this)">
							</div>
							<div class="col-md-12 text-center mb-3">
								<input type="submit"
									class=" btn btn-block mybtn btn-primary tx-tfm"
									value="Registrarse"></input>
							</div>
							<div class="col-md-12 ">
								<div class="form-group">
									<p class="text-center">
										<a href="login.jsp">¿Ya tienes una cuenta?</a>
									</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	
	
	<c:choose>
		<c:when test="${param.status == 1}">
			<p>El email ya está en uso</p>
		</c:when>
		<c:when test="${param.status == 2}">
			<p>Error del sistema, contacte con el administrador</p>
		</c:when>
	</c:choose>
</body>
</html>
