<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<sql:query var="modelos" dataSource="jdbc/classicmodels">
    select productCode, productName, productScale, productDescription, MSRP from products;
</sql:query>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<title>Catálogo de Productos</title>

 <style>



</style>

</head>
<body>

<div class="container-fluid ">

	<div class="jumbotron jumbotron-fluid ">
	
		<p class="display-4 align-self-center">Catálogo de Modelos a Escala</p>
	
	</div>
	
</div>

<ul class="nav justify-content-center">

<c:choose>

	<c:when test="${not empty sessionScope.customer }">
	
		<li class="nav-item">
	
			<p class="nav-link btn-secondary">${sessionScope.customer }</p>
			
		</li>	
		
		<li class="nav-item">
		
			<a class="nav-link " href="cart.jsp">Carrito</a>
			
		</li>
		
		<li class="nav-item">
		
			<a class="nav-link" href="logout.jsp" >Cerrar Sesion</a>
			
		</li>

	</c:when>
	<c:otherwise>
	
		<li class="nav-item">
	
			<a class="nav-link" href="login.jsp" >Iniciar Sesion</a>
			
		</li>
		
		<li class="nav-item">
			
			<a class="nav-link" href="registro.jsp" >Registrarse</a>
			
		</li>

	</c:otherwise>

</c:choose>

</ul>
	
<hr/>

<div class="container-fluid">

<div class="row">
	<c:forEach var="modelo" items="${modelos.rows}">
	
	<div class="card" style="width: 18rem;">
	
		<div class="card-body">
		
			<h5 class="card-title"><c:out value="${modelo.productName}" /></h5>
			
			<h6 class="card-subtitle mb-2 text-muted">Escala <c:out value="${modelo.productScale}" /></h6>
			
			<details>
			
			<summary>Descripcion</summary>
			
			<p class="card-text"><c:out value="${modelo.productDescription}" /></p>
			
			</details>
			
			<p>Precio: <c:out value="${modelo.MSRP}" />€</p>
			
			<c:if test="${not empty sessionScope.customer}">
				<a class="card-link btn btn-secondary" href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}">
					Añadir al carrito<img style="width: 1em; height: 1em;" src="http://simpleicon.com/wp-content/uploads/cart-plus.svg" />
				</a>
			</c:if>
		
		</div>
	
	</div>

	&nbsp
		
	</c:forEach>
	
</div></div>

</body>
</html>