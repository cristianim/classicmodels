<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true"%>

<c:if test="${not empty sessionScope.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<title>Inicio de Sesi�n</title>
</head>

<style>
.hr-or {
	height: 1px;
	margin-top: 0px !important;
	margin-bottom: 0px !important;
}

.login-or {
	position: relative;
	color: #aaa;
	margin-top: 10px;
	margin-bottom: 10px;
	padding-top: 10px;
	padding-bottom: 10px;
}

.span-or {
	display: block;
	position: absolute;
	left: 50%;
	top: -2px;
	margin-left: -25px;
	background-color: #fff;
	width: 50px;
	text-align: center;
}
</style>
<body>

	<div class="container-fluid">

		<div class="jumbotron jumbotron-fluid ">

			<p class="display-4">Inicia sesion</p>

		</div>

	</div>

	<ul class="nav justify-content-center">

		<c:choose>

			<c:when test="${not empty sessionScope.customer }">

				<li class="nav-item">

					<p class="nav-link btn-secondary">${sessionScope.customer }</p>

				</li>

				<li class="nav-item"><a class="nav-link " href="cart.jsp">Carrito</a>

				</li>

				<li class="nav-item"><a class="nav-link" href="logout.jsp">Cerrar
						Sesion</a></li>

			</c:when>
			<c:otherwise>

				<li class="nav-item"><a class="nav-link" href="catalogo.jsp">Volver
						al catalogo</a></li>

			</c:otherwise>

		</c:choose>

	</ul>

	<hr />


		<div class="container">
			<div class="row">
				<div class="col-md-5 mx-auto">
					<div id="first">
						<div class="myform form ">
							<form action="auth" method="post">
								<div class="form-group">
									<label for="exampleInputEmail1">Correo</label> <input
										type="email" name="email" class="form-control" id="email"
										aria-describedby="emailHelp" placeholder="Introduce el correo"
										required="required">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Contrase�a</label> <input
										type="password" name="password" id="password"
										class="form-control" aria-describedby="emailHelp"
										placeholder="Introduce la contrase�a" required="required">
								</div>
								<div class="col-md-12 text-center ">
									<button type="submit"
										class="btn btn-block mybtn btn-primary tx-tfm">Login</button>
								</div>
								<div class="col-md-12 ">
									<div class="login-or">
										<hr class="hr-or">
										<span class="span-or">o</span>
									</div>
								</div>
								<div class="col-md-12 mb-3">
									<p class="text-center">
										<a class="nav-link btn btn-block mybtn btn-secondary tx-tfm"
											href="registro.jsp"> Registrate </a>
									</p>
								</div>
							</form>

						</div>
					</div>

				</div>
			</div>

		<c:choose>
			<c:when test="${param.status == 1}">
				<p>Error, intentalo otra vez</p>
			</c:when>
			<c:when test="${param.status == 2}">
				<p>Error del servidor</p>
			</c:when>
		</c:choose>

	</div>

</body>
</html>