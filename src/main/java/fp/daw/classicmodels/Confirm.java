package fp.daw.classicmodels;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;


@WebServlet("/confirm")

public class Confirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("usuario") != null)
			response.sendRedirect("catalogo.jsp");
		else {
			String cc = request.getParameter("cc");
			String email = request.getParameter("email");
			session.setAttribute("cc", cc);
			confirmacion(cc, email);
			response.sendRedirect("login.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	public static void confirmacion(String cc, String email) {
		Connection con = null;
		PreparedStatement stm = null;
		ResultSet rs = null;

			try {
				Context context = new InitialContext();
				DataSource ds = (DataSource) context.lookup("java:comp/env/jdbc/classicmodels");
				con = ds.getConnection();
				stm = con.prepareStatement("update signups set confirmationCode = ? where confirmationCode= ? and customerEmail = ?");
				stm.setNull(1, 1);
				stm.setString(2, cc);
				stm.setString(3, email);
				stm.executeUpdate();
			} catch (NamingException | SQLException e) {
				e.printStackTrace();
			}	finally {
				if (rs != null)
					try {
						rs.close();
					} catch (SQLException e) {
					}
				if (stm != null)
					try { stm.close(); } catch (SQLException e) {}
				if (con != null)
					try { con.close(); } catch (SQLException e) {}
			}
			
			
			
	}

}
